
-- INTRODUCTION --

This module keeps listing of all permission assigned to every user role.

-- REQUIREMENTS --
  No

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:


-- CUSTOMIZATION --

* Access menu from here : admin/people/rolewise-permissions
